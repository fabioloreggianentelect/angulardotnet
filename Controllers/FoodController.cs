using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace AngularDotNet.Controllers
{
    [Route("api/[controller]")]
    public class FoodController : Controller
    {
        private static string[] Names = new[]
        {
            "Pizza", "Pasta", "Taco Bell", "Hamburger", "Soup", "Steak", "French Fries"
        };

        [HttpGet("[action]")]
        public IEnumerable<Food> Items()
        {
            var rng = new Random();
            return Enumerable.Range(1, 5).Select(index => new Food
            {
                Id = Guid.NewGuid(),
                Amount = rng.Next(0, 55),
                Name = Names[rng.Next(Names.Length)]
            });
        }

        public class Food
        {
            public Guid  Id { get; set; }
            public int Amount { get; set; }
            public string Name { get; set; }
        }
    }
}
