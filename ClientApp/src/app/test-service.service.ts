import { Injectable, Inject } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class TestServiceService {
  public forecasts$: BehaviorSubject<WeatherForecast[]>;
  public foods$: BehaviorSubject<Food[]>;

  constructor(private http: HttpClient, @Inject('BASE_URL') private baseUrl: string) {
    this.forecasts$ = new BehaviorSubject<WeatherForecast[]>([]);
    this.foods$ = new BehaviorSubject<Food[]>([]);

    this.getData();
  }

  getData(): void {
    this.http.get<WeatherForecast[]>(this.baseUrl + 'api/SampleData/WeatherForecasts').subscribe(result => {
      this.forecasts$.next(result);
    }, error => console.error(error));

    this.http.get<Food[]>(this.baseUrl + 'api/Food/Items').subscribe(result => {
      this.foods$.next(result);
    }, error => console.error(error));
  }
}

export interface WeatherForecast {
  dateFormatted: string;
  temperatureC: number;
  temperatureF: number;
  summary: string;
}

export interface Food {
  id: string;
  name: string;
  amount: number;
}
